# Pris System

Main system, boot and splash partition images of Project Pris (GerdaOS)

## Supported builds as of now

- `boot`
- `system`
- `splash`
- `installer`

## Dependency requirements

### Overall

- ADB
- Bash 3+
- Make
- JRE8

### For building the boot image

- Make, GCC

### For building the system image

- `make_ext4fs` (compiled during the build process)

### For building the splash image

- FFmpeg

## System image structure

_TODO_

## Building the images

```
make build
```

Files called `boot.new.img`, `system.new.img` and `splash.new.img` will be created in the project directory.

You can also individually run `make build-boot`, `make build-system` and `make build-splash` to build only the parts you need right now.

## Backing up existing partitions

```
make backup
```

Files called `boot.$RANDOMID.img`, `system.$RANDOMID.img` and `splash.$RANDOMID.img` will be created in the `works/backups/` directory.
The RANDOMID value will be consistent across all backup images.

You can also individually run `make backup-boot`, `make backup-system` and `make backup-splash` to backup only the parts you need right now.

## Deploying the images to the device

You must have Pris Recovery (aka Gerda Recovery) installed on your device for deployment to succeed.

Boot the phone into Pris Recovery and then run:

```
make deploy
```

You can also individually run `make deploy-boot`, `make deploy-system` and `make deploy-splash` to flash only the parts you need right now.


## Building the update package for recovery

Alternatively, you can also build a ready-made update.zip for the HMD firmware v12 or for Pris Recovery (highly recommended) over any stock system version.

```
make build-installer
```

or

```
make VERSION=${ver} build-installer
```

If the `VERSION` variable is omitted, short hash of the recent commit will be used instead. Same thing goes for GitLab CI automated build. 

A file called `gerda-install-${ver}.zip` will be created in the project directory.

Note that this file will not install Gerda Recovery! And if Gerda Recovery isn't installed, the update can run only from stock v12 and lower recoveries.

To install the image, run `adb reboot recovery`, select "Wipe data/factory reset" and then "Install update from ADB", afterwards run:

```
adb sideload gerda-install-${ver}.zip
```

And wait until the process completes. Afterwards, reboot the phone to see your newly installed OS.


## Changing boot splash image

Note: this guide only focuses on the splash partition image (which is "Powered by KaiOS" by default). The system one ("Nokia") is stored in `system` partition.

Prerequisites: `imagemagick` and/or `ffmpeg` package installed. Currently `ffmpeg` is required.

### Boot splash format description

- First 8 bytes (0 to 7): `SPLASH!!` (or `53 50 4C 41 53 48 21 21` in hex)
- Bytes 8 to 11: 32-bit width, little-endian (for 8110: `f0 00 00 00`, equals to 240)
- Bytes 12 to 15: 32-bit height, little-endian (for 8110: `40 01 00 00`, equals to 320)
- Bytes 16 to 19: 32-bit image type, little-endian (for 8110: `00 00 00 00`)
- Bytes 20 to 23: 32-bit 512-block count, little endian (for 8110: `2c 01 00 00`, equals to 240x320x2/512 = 300)
- Next 488 bytes: zero-filled
- Bytes from 512: raw 16-bit image data in BGR565LE format, length must be 153600 bytes

The header suitable for 8110 is stored in the `src/splash/logohdr.bin` file.

### Manual steps to build the splash image

1. Place your PNG with the logo to `src/splash/logo.png`.
2. Run:

```
ffmpeg -vcodec png -i src/splash/logo.png -vcodec rawvideo -f rawvideo -pix_fmt bgr565 -s 240x320 -y tmp.bin`
cat src/splash/logohdr.bin tmp.bin > splash.new.img
rm tmp.bin
```

### Automatic building

The splash image is built among others when running `make build`. To build a new one, place a 240x320 PNG file into `src/splash/logo.png`.
