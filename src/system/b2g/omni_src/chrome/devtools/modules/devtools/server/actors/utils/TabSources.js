"use strict";const{Ci,Cu}=require("chrome");const DevToolsUtils=require("devtools/shared/DevToolsUtils");const{assert,fetch}=DevToolsUtils;const EventEmitter=require("devtools/shared/event-emitter");const{OriginalLocation,GeneratedLocation}=require("devtools/server/actors/common");const{resolve}=require("promise");const{joinURI}=require("devtools/shared/path");const URL=require("URL");loader.lazyRequireGetter(this,"SourceActor","devtools/server/actors/source",true);loader.lazyRequireGetter(this,"isEvalSource","devtools/server/actors/source",true);loader.lazyRequireGetter(this,"SourceMapConsumer","source-map",true);loader.lazyRequireGetter(this,"SourceMapGenerator","source-map",true);function TabSources(threadActor,allowSourceFn=()=>true){EventEmitter.decorate(this);this._thread=threadActor;this._useSourceMaps=true;this._autoBlackBox=true;this._anonSourceMapId=1;this.allowSource=source=>{return!isHiddenSource(source)&&allowSourceFn(source);}
this.blackBoxedSources=new Set();this.prettyPrintedSources=new Map();this.neverAutoBlackBoxSources=new Set(); this._sourceMaps=new Map(); this._sourceMapCache=Object.create(null); this._sourceActors=new Map(); this._sourceMappedSourceActors=Object.create(null);}
const MINIFIED_SOURCE_REGEXP=/\bmin\.js$/;TabSources.prototype={setOptions:function(options){let shouldReset=false;if('useSourceMaps'in options){shouldReset=true;this._useSourceMaps=options.useSourceMaps;}
if('autoBlackBox'in options){shouldReset=true;this._autoBlackBox=options.autoBlackBox;}
if(shouldReset){this.reset();}},reset:function(opts={}){this._sourceActors=new Map();this._sourceMaps=new Map();this._sourceMappedSourceActors=Object.create(null);if(opts.sourceMaps){this._sourceMapCache=Object.create(null);}},source:function({source,originalUrl,generatedSource,isInlineSource,contentType}){assert(source||(originalUrl&&generatedSource),"TabSources.prototype.source needs an originalUrl or a source");if(source){
if(!this.allowSource(source)){return null;}



if(source.url in this._sourceMappedSourceActors){return this._sourceMappedSourceActors[source.url];}
if(isInlineSource){


originalUrl=source.url;source=null;}
else if(this._sourceActors.has(source)){return this._sourceActors.get(source);}}
else if(originalUrl){


for(let[source,actor]of this._sourceActors){if(source.url===originalUrl){return actor;}}
if(originalUrl in this._sourceMappedSourceActors){return this._sourceMappedSourceActors[originalUrl];}}
let actor=new SourceActor({thread:this._thread,source:source,originalUrl:originalUrl,generatedSource:generatedSource,isInlineSource:isInlineSource,contentType:contentType});let sourceActorStore=this._thread.sourceActorStore;var id=sourceActorStore.getReusableActorId(source,originalUrl);if(id){actor.actorID=id;}
this._thread.threadLifetimePool.addActor(actor);sourceActorStore.setReusableActorId(source,originalUrl,actor.actorID);if(this._autoBlackBox&&!this.neverAutoBlackBoxSources.has(actor.url)&&this._isMinifiedURL(actor.url)){this.blackBox(actor.url);this.neverAutoBlackBoxSources.add(actor.url);}
if(source){this._sourceActors.set(source,actor);}
else{this._sourceMappedSourceActors[originalUrl]=actor;}
this._emitNewSource(actor);return actor;},_emitNewSource:function(actor){if(!actor.source){

this.emit('newSource',actor);}
else{






this.fetchSourceMap(actor.source).then(map=>{if(!map){this.emit('newSource',actor);}});}},getSourceActor:function(source){if(source.url in this._sourceMappedSourceActors){return this._sourceMappedSourceActors[source.url];}
if(this._sourceActors.has(source)){return this._sourceActors.get(source);}
throw new Error('getSource: could not find source actor for '+
(source.url||'source'));},getSourceActorByURL:function(url){if(url){for(let[source,actor]of this._sourceActors){if(source.url===url){return actor;}}
if(url in this._sourceMappedSourceActors){return this._sourceMappedSourceActors[url];}}
throw new Error('getSourceByURL: could not find source for '+url);return null;},_isMinifiedURL:function(aURL){try{let url=new URL(aURL);let pathname=url.pathname;return MINIFIED_SOURCE_REGEXP.test(pathname.slice(pathname.lastIndexOf("/")+1));}catch(e){
return MINIFIED_SOURCE_REGEXP.test(aURL);}},createNonSourceMappedActor:function(aSource){




let url=isEvalSource(aSource)?null:aSource.url;let spec={source:aSource};





let element=aSource.element?aSource.element.unsafeDereference():null;if(element&&(element.tagName!=="SCRIPT"||!element.hasAttribute("src"))){spec.isInlineSource=true;}else if(aSource.introductionType==="wasm"){spec.contentType="text/wasm";}else{if(url){ if(url.indexOf("Scratchpad/")===0||url.indexOf("javascript:")===0||url==="debugger eval code"){spec.contentType="text/javascript";}else{try{let pathname=new URL(url).pathname;let filename=pathname.slice(pathname.lastIndexOf("/")+1);let index=filename.lastIndexOf(".");let extension=index>=0?filename.slice(index+1):"";if(extension==="xml"){

spec.isInlineSource=true;}
else if(extension==="js"){spec.contentType="text/javascript";}}catch(e){
const filename=url;const index=filename.lastIndexOf(".");const extension=index>=0?filename.slice(index+1):"";if(extension==="js"){spec.contentType="text/javascript";}}}}
else{ spec.contentType="text/javascript";}}
return this.source(spec);},_createSourceMappedActors:function(aSource){if(!this._useSourceMaps||!aSource.sourceMapURL){return resolve(null);}
return this.fetchSourceMap(aSource).then(map=>{if(map){return map.sources.map(s=>{return this.source({originalUrl:s,generatedSource:aSource});}).filter(isNotNull);}
return null;});},createSourceActors:function(aSource){return this._createSourceMappedActors(aSource).then(actors=>{let actor=this.createNonSourceMappedActor(aSource);return(actors||[actor]).filter(isNotNull);});},fetchSourceMap:function(aSource){if(this._sourceMaps.has(aSource)){return this._sourceMaps.get(aSource);}
else if(!aSource||!aSource.sourceMapURL){return resolve(null);}
let sourceMapURL=aSource.sourceMapURL;if(aSource.url){sourceMapURL=joinURI(aSource.url,sourceMapURL);}
let result=this._fetchSourceMap(sourceMapURL,aSource.url);

this._sourceMaps.set(aSource,result);return result;},getSourceMap:function(aSource){return resolve(this._sourceMaps.get(aSource));},setSourceMap:function(aSource,aMap){this._sourceMaps.set(aSource,resolve(aMap));},_fetchSourceMap:function(aAbsSourceMapURL,aSourceURL){if(!this._useSourceMaps){return resolve(null);}
else if(this._sourceMapCache[aAbsSourceMapURL]){return this._sourceMapCache[aAbsSourceMapURL];}
let fetching=fetch(aAbsSourceMapURL,{loadFromCache:false}).then(({content})=>{let map=new SourceMapConsumer(content);this._setSourceMapRoot(map,aAbsSourceMapURL,aSourceURL);return map;}).then(null,error=>{if(!DevToolsUtils.reportingDisabled){DevToolsUtils.reportException("TabSources.prototype._fetchSourceMap",error);}
return null;});this._sourceMapCache[aAbsSourceMapURL]=fetching;return fetching;},_setSourceMapRoot:function(aSourceMap,aAbsSourceMapURL,aScriptURL){
if(aSourceMap.hasContentsOfAllSources()){return;}
const base=this._dirname(aAbsSourceMapURL.indexOf("data:")===0?aScriptURL:aAbsSourceMapURL);aSourceMap.sourceRoot=aSourceMap.sourceRoot?joinURI(base,aSourceMap.sourceRoot):base;},_dirname:function(aPath){let url=new URL(aPath);let href=url.href;return href.slice(0,href.lastIndexOf("/"));},clearSourceMapCache:function(aSourceMapURL,opts={hard:false}){let oldSm=this._sourceMapCache[aSourceMapURL];if(opts.hard){delete this._sourceMapCache[aSourceMapURL];}
if(oldSm){ for(let[source,sm]of this._sourceMaps.entries()){if(sm===oldSm){this._sourceMaps.delete(source);}}}},setSourceMapHard:function(aSource,aUrl,aMap){let url=aUrl;if(!url){




url="internal://sourcemap"+(this._anonSourceMapId++)+'/';}
aSource.sourceMapURL=url;
this._sourceMapCache[url]=resolve(aMap);this.emit("updatedSource",this.getSourceActor(aSource));},getFrameLocation:function(aFrame){if(!aFrame||!aFrame.script){return new GeneratedLocation();}
let{lineNumber,columnNumber}=aFrame.script.getOffsetLocation(aFrame.offset);return new GeneratedLocation(this.createNonSourceMappedActor(aFrame.script.source),lineNumber,columnNumber);},getOriginalLocation:function(generatedLocation){let{generatedSourceActor,generatedLine,generatedColumn}=generatedLocation;let source=generatedSourceActor.source;let url=source?source.url:generatedSourceActor._originalUrl;



return this.fetchSourceMap(source).then(map=>{if(map){let{source:originalUrl,line:originalLine,column:originalColumn,name:originalName}=map.originalPositionFor({line:generatedLine,column:generatedColumn==null?Infinity:generatedColumn});




return new OriginalLocation(originalUrl?this.source({originalUrl:originalUrl,generatedSource:source}):null,originalLine,originalColumn,originalName);} 
return OriginalLocation.fromGeneratedLocation(generatedLocation);});},getAllGeneratedLocations:function(originalLocation){let{originalSourceActor,originalLine,originalColumn}=originalLocation;let source=(originalSourceActor.source||originalSourceActor.generatedSource);return this.fetchSourceMap(source).then((map)=>{if(map){map.computeColumnSpans();return map.allGeneratedPositionsFor({source:originalSourceActor.url,line:originalLine,column:originalColumn}).map(({line,column,lastColumn})=>{return new GeneratedLocation(this.createNonSourceMappedActor(source),line,column,lastColumn);});}
return[GeneratedLocation.fromOriginalLocation(originalLocation)];});},getGeneratedLocation:function(originalLocation){let{originalSourceActor}=originalLocation;

let source=originalSourceActor.source||originalSourceActor.generatedSource;return this.fetchSourceMap(source).then((map)=>{if(map){let{originalLine,originalColumn}=originalLocation;let{line:generatedLine,column:generatedColumn}=map.generatedPositionFor({source:originalSourceActor.url,line:originalLine,column:originalColumn==null?0:originalColumn,bias:SourceMapConsumer.LEAST_UPPER_BOUND});return new GeneratedLocation(this.createNonSourceMappedActor(source),generatedLine,generatedColumn);}
return GeneratedLocation.fromOriginalLocation(originalLocation);});},isBlackBoxed:function(aURL){return this.blackBoxedSources.has(aURL);},blackBox:function(aURL){this.blackBoxedSources.add(aURL);},unblackBox:function(aURL){this.blackBoxedSources.delete(aURL);},isPrettyPrinted:function(aURL){return this.prettyPrintedSources.has(aURL);},prettyPrint:function(aURL,aIndent){this.prettyPrintedSources.set(aURL,aIndent);},prettyPrintIndent:function(aURL){return this.prettyPrintedSources.get(aURL);},disablePrettyPrint:function(aURL){this.prettyPrintedSources.delete(aURL);},iter:function(){let actors=Object.keys(this._sourceMappedSourceActors).map(k=>{return this._sourceMappedSourceActors[k];});for(let actor of this._sourceActors.values()){if(!this._sourceMaps.has(actor.source)){actors.push(actor);}}
return actors;}};function isHiddenSource(aSource){ return aSource.text==='() {\n}';}
function isNotNull(aThing){return aThing!==null;}
exports.TabSources=TabSources;exports.isHiddenSource=isHiddenSource;