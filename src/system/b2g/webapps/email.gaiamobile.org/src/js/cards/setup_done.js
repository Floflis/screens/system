define([ "require", "evt", "./base", "template!./setup_done.html" ], function(e) {
    var t, n = e("evt");
    return [ e("./base")(e("template!./setup_done.html")), {
        onArgs: function() {
            t = this;
        },
        onCardVisible: function() {
            var e = [ {
                name: "Add Account",
                l10nId: "settings-account-add",
                priority: 1,
                method: function() {
                    t.onAddAnother();
                }
            }, {
                name: "Finish",
                l10nId: "finish",
                priority: 3,
                method: function() {
                    t.onShowMail();
                }
            } ];
            NavigationMap.setSoftKeyBar(e);
            var n = document.getElementsByTagName("cards-setup-done")[0];
            n.setAttribute("role", "heading"), n.setAttribute("aria-labelledby", "setup-done-header"), 
            this.msgNode.focus();
        },
        onAddAnother: function() {
            n.emit("addAccount");
        },
        onShowMail: function() {
            n.emit("showLatestAccount");
        },
        die: function() {}
    } ];
});