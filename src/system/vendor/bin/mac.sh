#!/system/bin/busybox sh

# Usage: qpwn.sh [sim number] [new IMEI - 15 decimals]
#MAC change 
OPDIR=$(mktemp -d)
BLKPREF=/dev/block/bootdevice/by-name
MAC=$1
TARGETNAME=nvm/num/4678
PREP="$(echo -n "${MAC//:}" | busybox sed -re 's/([0-9a-f])([0-9a-f])/\\x\1\2/g')"
cd $OPDIR
echo "Reading tunning partition..."
busybox tar xf $BLKPREF/tunning
echo -ne "$PREP" > $TARGETNAME 
echo "Writing tunning partition..."
busybox tar cf - . > $BLKPREF/tunning
echo "Formatting modemst1 and modemst2..."
dd if=/dev/zero of=$BLKPREF/modemst1
dd if=/dev/zero of=$BLKPREF/modemst2
echo "MAC changed, reboot to apply"
